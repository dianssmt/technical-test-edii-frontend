import React from 'react';
import {
    Button, Card, Form, CardHeader, CardBody, FormGroup, Label, Input, Col, CardFooter
} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import './../../../assets/icon.css'

class KaryawanPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idRequestBooking: '',
            description: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    initialInput = {
        idRequestBooking: '',
        description: '',
    }

    submitForm = (event) => {
        event.preventDefault();
        const data = {
            idRequestBooking: this.state.idRequestBooking,
            description: this.state.description,
        }
        axios.post("http://localhost:9090/images", data)
            .then(response => {
                console.log(response)
            })
        window.location.href = "#/log/table"
    }

handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
}

render() {
    const { idRequestBooking, description } = this.state;

    return (
        <Card className="main-card mb-3">
            <CardHeader>
                <h6>Add Images</h6>
            </CardHeader>
            <CardBody>
                <Form
                    onSubmit={this.submitForm}>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="idRequestBooking" sm='2'>ID Request Booking</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="idRequestBooking"
                                id="idRequestBooking"
                                autoComplete='off'
                                value={idRequestBooking}
                                onChange={this.handleChange} />
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="description" sm='2'>ID Platform</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="description"
                                id="description"
                                autoComplete='off'
                                value={description}
                                onChange={this.handleChange} />
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Col sm='2'>
                            <Button
                                className="btn-lg btn-shadow mt-1"
                                style={{ backgroundColor: "#ffffff", border: "none", color: "currentcolor" }}
                                href="#/log/table">
                                Kembali
                            </Button>
                        </Col>
                        <Col sm='2'>
                            <Button
                                className="btn-lg btn-shadow mt-1"
                                style={{ background: "#C0D39A", border: "none", color: "currentcolor" }}
                                type='submit'>
                                Simpan
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </CardBody>
            <CardFooter />
        </Card>
    );
}
}

export default KaryawanPost;

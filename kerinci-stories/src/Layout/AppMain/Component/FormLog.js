import React from 'react';
import {
    Button, Card, Form, CardHeader, CardBody, FormGroup, Label, Input, Col, CardFooter
} from 'reactstrap';
import axios from 'axios';
import './../../../assets/icon.css'

class KaryawanPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idRequestBooking: '',
            idPlatform: '',
            namaPlatform: '',
            docType: '',
            termOfPayment: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    initialInput = {
        idRequestBooking: '',
        idPlatform: '',
        namaPlatform: '',
        docType: '',
        termOfPayment: '',
    }

    submitForm = (event) => {
        event.preventDefault();
        const data = {
            idRequestBooking: this.state.idRequestBooking,
            idPlatform: this.state.idPlatform,
            namaPlatform: this.state.namaPlatform,
            docType: this.state.docType,
            termOfPayment: this.state.termOfPayment,
        }
        axios.post("http://localhost:9090/log", data)
            .then(response => {
                console.log(response)
            })
        window.location.href = "#/log/table"
    }

handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
}

render() {
    const { idRequestBooking, idPlatform, namaPlatform, docType, termOfPayment } = this.state;

    return (
        <Card className="main-card mb-3">
            <CardHeader>
                <h6>Add Log</h6>
            </CardHeader>
            <CardBody>
                <Form
                    onSubmit={this.submitForm}>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="idRequestBooking" sm='2'>ID Request Booking</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="idRequestBooking"
                                id="idRequestBooking"
                                autoComplete='off'
                                value={idRequestBooking}
                                onChange={this.handleChange} />
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="idPlatform" sm='2'>ID Platform</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="idPlatform"
                                id="idPlatform"
                                autoComplete='off'
                                value={idPlatform}
                                onChange={this.handleChange} />
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="namaPlatform" sm='2' >Nama Platform</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="namaPlatform"
                                id="namaPlatform"
                                autoComplete='off'
                                value={namaPlatform}
                                onChange={this.handleChange}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="docType" sm='2' >Doc Type</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="docType"
                                id="docType"
                                autoComplete='off'
                                value={docType}
                                onChange={this.handleChange}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Label for="termOfPayment" sm='2' >Term of Payment</Label>
                        <Col sm='5'>
                            <Input
                                type="text"
                                name="termOfPayment"
                                id="termOfPayment"
                                autoComplete='off'
                                value={termOfPayment}
                                onChange={this.handleChange}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row style={{ justifyContent: "center" }}>
                        <Col sm='2'>
                            <Button
                                className="btn-lg btn-shadow mt-1"
                                style={{ backgroundColor: "#ffffff", border: "none", color: "currentcolor" }}
                                href="#/log/table">
                                Kembali
                            </Button>
                        </Col>
                        <Col sm='2'>
                            <Button
                                className="btn-lg btn-shadow mt-1"
                                style={{ background: "#C0D39A", border: "none", color: "currentcolor" }}
                                type='submit'>
                                Simpan
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </CardBody>
            <CardFooter />
        </Card>
    );
}
}

export default KaryawanPost;

import React, { Fragment } from 'react'
import {
    Card, CardBody, CardHeader,
    Button,
    CardFooter, Row, Col
} from 'reactstrap';
import ReactTable from "react-table";
import axios from 'axios';

export default class Dam extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            log: [],
            images: [],
        };
        this.toggleLog = this.toggleLog.bind(this);
        this.toggleImages = this.toggleImages.bind(this);
    }

    toggleLog = () => {
        window.location.href = "#/log/formlog"
    }

    toggleImages = (id) => {
        window.location.href = "#/images/formimages"
    }

    componentDidMount = async () => {
        await axios.get("http://localhost:9090/log").then(({ data }) => this.setState({ log: data.data }));
        await axios.get("http://localhost:9090/images").then(({ data }) => this.setState({ images: data.data }));
    }

    render() {
        const { log, images } = this.state;
        return (
            <Fragment>
                <Row>
                    <Col md="7">
                        <Card className="main-card mb-3">
                            <CardHeader>
                                <h6>Log Table</h6>
                            </CardHeader>
                            <CardBody>
                                <div className="btn-actions-pane-right">
                                    <Button
                                        className="btn-md btn-shadow btn-shine text-right mr-3"
                                        onClick={this.toggleLog} style={{ border: "none", marginTop: '-10px', marginBottom: '10px' }}>
                                        Tambah
                            </Button>
                                </div>
                                {this.state.log ?
                                    <ReactTable
                                        data={log}
                                        columns={[{
                                            columns: [
                                                {
                                                    Header: 'No',
                                                    accessor: 'idRequestBooking',
                                                    width: 45,
                                                    Cell: row => (
                                                        <div className='d-block w-100 text-center'>
                                                            {row.value}
                                                        </div>)
                                                },
                                                {
                                                    Header: 'ID Platform',
                                                    accessor: 'idPlatform'
                                                },
                                                {
                                                    Header: 'Nama Platform',
                                                    accessor: 'namaPlatform',
                                                    Cell: row => (
                                                        <div className='d-block w-100 text-center'>
                                                            {row.value}
                                                        </div>)
                                                },
                                                {
                                                    Header: 'Doc Type',
                                                    accessor: 'docType'
                                                },
                                                {
                                                    Header: 'Term of Payment',
                                                    accessor: 'termOfPayment',
                                                }
                                            ]
                                        },
                                        ]}
                                        defaultPageSize={10}
                                        className="-striped -highlight"
                                    />
                                    : ""}
                            </CardBody>
                            <CardFooter />
                        </Card>
                    </Col>
                    <Col md="5">
                        <Card className="main-card mb-3">
                            <CardHeader>
                                <h6>Images Table</h6>
                            </CardHeader>
                            <CardBody>
                                <div className="btn-actions-pane-right">
                                    <Button
                                        className="btn-md btn-shadow btn-shine text-right mr-3"
                                        onClick={this.toggleImages} style={{ border: "none", marginTop: '-10px', marginBottom: '10px' }}>
                                        Tambah
                            </Button>
                                </div>
                                {this.state.images ?
                                    <ReactTable
                                        data={images}
                                        columns={[{
                                            columns: [
                                                {
                                                    Header: 'No',
                                                    accessor: 'idRequestBooking',
                                                    width: 45,
                                                    Cell: row => (
                                                        <div className='d-block w-100 text-center'>
                                                            {row.value}
                                                        </div>)
                                                },
                                                {
                                                    Header: 'Description',
                                                    accessor: 'description'
                                                }
                                            ]
                                        },
                                        ]}
                                        defaultPageSize={10}
                                        className="-striped -highlight"
                                    />
                                    : ""}
                            </CardBody>
                            <CardFooter />
                        </Card>
                    </Col>
                </Row>
            </Fragment>
        )
    }
}
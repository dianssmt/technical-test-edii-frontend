import { Route, Redirect } from 'react-router-dom';
import React, { Suspense, lazy, Fragment } from 'react';
import { Container, Row, Col } from 'reactstrap';

const FormLog = lazy(() => import('./Component/FormLog'))
const FormImages = lazy(() => import('./Component/FormImages'))
const Table = lazy(() => import('./Component/Table'))

const AppMain = () => {
    return (
        <Fragment>
            <Container>
                <h4
                    style={{
                        textAlign: 'center',
                        marginTop: '20px',
                        marginBottom: '20px'
                    }}>
                    Master Log & Images
                </h4>
                <Row>
                    <Col>
                        <Suspense fallback={
                            <div className="loader-container">
                                <div className="loader-container-inner" />
                            </div>
                        }>
                            <Route path='/log/formlog' component={FormLog} />
                        </Suspense>
                        <Suspense fallback={
                            <div className="loader-container">
                                <div className="loader-container-inner" />
                            </div>
                        }>
                            <Route path='/log/table' component={Table} />
                        </Suspense>
                        <Suspense fallback={
                            <div className="loader-container">
                                <div className="loader-container-inner" />
                            </div>
                        }>
                            <Route path='/images/formimages' component={FormImages} />
                        </Suspense>

                        <Route exact path="/" render={() => (
                            <Redirect to="/log/table" />
                        )} />
                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
};

export default AppMain;